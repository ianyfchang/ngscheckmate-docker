FROM biocontainers/biocontainers:latest


RUN conda install -c bioconda -c conda-forge r-base samtools=1.3.1 bcftools=1.3.1

USER root

ENV NCM_HOME=/opt/NGSCheckMate
ENV PATH="${NCM_HOME}:${PATH}"
WORKDIR /opt
RUN git clone https://github.com/parklab/NGSCheckMate.git && \
    chmod a+x $NCM_HOME/*.py && \
    sed -i '1i#!/usr/bin/env python' $NCM_HOME/*.py

